﻿using Soap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1_soapClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("press return ... ");
            Console.ReadLine();

            BookServiceClient client = new BookServiceClient();

            foreach (BookDTO b in client.GetBooks())
            {
                Console.WriteLine(b.Title);
            }
            Console.WriteLine();

            BookDTO book = new BookDTO()
            {
                Bookno = 13,
                Title = "The Da Vinci Code",
                Author = "Dan Brown"
            };

            client.AddBooks(book);

            foreach (BookDTO b in client.GetBooks())
            {
                Console.WriteLine(b.Title);
            }

            client.DelBooks(book.Bookno);
            Console.WriteLine();
            foreach (BookDTO b in client.GetBooks())
            {
                Console.WriteLine(b.Title);
            }



            Console.ReadLine();
        }
    }
}
