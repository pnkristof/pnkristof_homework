﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Web.DTO;

namespace Web.Controllers
{
    public class BookController : ApiController
    {
        ILogic logic;
        public BookController()
        {
            logic = new RealLogic();
        }


        

        // get api/book/all
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<BookDTO> GetAllBooks()
        {
            var list = logic.GetBooks();
            return BookDTO.Mapper.Map<List<Book>, IEnumerable<BookDTO>>(list);
        }
    }
}
