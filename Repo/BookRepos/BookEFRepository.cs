﻿
using Data;
using Repo.GenericRepos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repo.BookRepos
{
    public class BookEFRepository : EFRepository<BOOK>, IBookRepository
    {
        public BookEFRepository(DbContext ctx) : base(ctx)
        {

        }

        public override BOOK GetById(int id)
        {
            return Get(x => x.BOOKNO == id).SingleOrDefault();
        }

        public void Modify(int id, string newtitle = null, string newauthor = null)
        {
            BOOK akt = GetById(id);
            if (akt == null) throw new ArgumentException("NO DATA");
            if (newtitle != null) akt.TITLE = newtitle;
            if (newauthor != null) akt.AUTHOR = newauthor;
            context.SaveChanges();
        }
    }
}

