﻿using Data;
using Repo.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.BookRepos
{
    public interface IBookRepository : IRepository<BOOK>
    {

        void Modify(int id, string newtitle, string newauthor);

    }
}
