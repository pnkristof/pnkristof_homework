﻿using Repo.BookRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repo.GenericRepos
{
    public class MyRepository
    {
        
        public IBookRepository BookRepo { get; private set; }

        public MyRepository(IBookRepository newbook)
        {
            BookRepo = newbook;
            
        }
    }
}
