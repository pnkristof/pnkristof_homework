﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repo.GenericRepos
{
    public abstract class EFRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        public abstract TEntity GetById(int id);

        protected DbContext context;
        public EFRepository(DbContext ctx)
        {
            context = ctx;
        }

        public void Delete(int id)
        {
            TEntity entity = GetById(id);
            if (entity == null) throw new ArgumentException("No Data");
            Delete(entity);
        }

        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> condition)
        {
            return GetAll().Where(condition);
        }

        public IQueryable<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public void Insert(TEntity entity)
        {
            context.Set<TEntity>().Add(entity);
            context.SaveChanges();
        }
    }
}
