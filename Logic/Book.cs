﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class Book
    {
        public decimal BOOKNO { get; set; }
        public string TITLE { get; set; }
        public string AUTHOR { get; set; }
    }
}
