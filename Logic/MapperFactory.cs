﻿using AutoMapper;
using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    class MapperFactory
    {

        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BOOK, Book>().ReverseMap();
                
            });
            return config.CreateMapper();
        }

    }
}
