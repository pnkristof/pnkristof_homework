﻿using AutoMapper;
using Data;
using Repo.BookRepos;
using Repo.GenericRepos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public class RealLogic : ILogic
    {
        protected MyRepository repo;
        IMapper mapper;

        public RealLogic()
        {
            BookEntities BE = new BookEntities();

            var bookRepo = new BookEFRepository(BE);
            repo = new MyRepository(bookRepo);
            mapper = MapperFactory.CreateMapper();
        }

        public RealLogic(IBookRepository bookRepo)
        {
            repo = new MyRepository(bookRepo);
            mapper = MapperFactory.CreateMapper();
        }

        public void AddBook(Book newbook)
        {
            BOOK b = mapper.Map<Book, BOOK>(newbook);
            repo.BookRepo.Insert(b);
        }

        public void DelBook(int id)
        {
            repo.BookRepo.Delete(id);
        }

        public List<Book> GetBooks()
        {
            IQueryable<BOOK> list = repo.BookRepo.GetAll();
            return mapper.Map<IQueryable<BOOK>, List<Book>>(list);
        }

        public Book GetOneBook(int id)
        {
            BOOK d = repo.BookRepo.GetById(id);
            return mapper.Map<BOOK, Book>(d);
        }

        public void ModifyBook(int id, string title, string auth)
        {
            repo.BookRepo.Modify(id, title, auth);
        }

        
    }
}
