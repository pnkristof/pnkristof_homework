﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logic
{
    public interface ILogic
    {

        List<Book> GetBooks();


        void AddBook(Book newbook);
        void DelBook(int id);
        Book GetOneBook(int id);
        void ModifyBook(int id, string title, string auth);
    }
}
