﻿DROP SEQUENCE BOOKSEQ
CREATE SEQUENCE BOOKSEQ AS int START WITH 10 INCREMENT BY 10;

DROP TABLE BOOK
CREATE TABLE BOOK
    (BOOKNO		NUMERIC(2) NOT NULL,
     TITLE               VARCHAR(20),
     AUTHOR                 VARCHAR(40),
  CONSTRAINT BOOK_PRIMARY_KEY PRIMARY KEY (BOOKNO));

INSERT INTO BOOK VALUES (NEXT VALUE FOR BOOKSEQ, 'HARRY POTTER', 'JOANNE KATHLEEN ROWLING');
INSERT INTO BOOK VALUES (NEXT VALUE FOR BOOKSEQ, 'THE HOBBIT', 'JOHN RONALD REUEL TOLKIEN');
INSERT INTO BOOK VALUES (NEXT VALUE FOR BOOKSEQ, 'IT', 'STEPHEN KING');
INSERT INTO BOOK VALUES (NEXT VALUE FOR BOOKSEQ, 'A GAME OF THRONES', 'GEORGE RAYMOND RICHARD MARTIN');
GO