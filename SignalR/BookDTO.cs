﻿using AutoMapper;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR
{
    public class BookDTO
    {

        public static IMapper Mapper { get; private set; }

        static BookDTO()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BookDTO, Book>().ReverseMap();
            }).CreateMapper();
        }


        public int Bookno { get; set; }

        public string Title{ get; set; }

        public string Author { get; set; }
    }
}
