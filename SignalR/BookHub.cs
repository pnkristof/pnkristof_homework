﻿using Logic;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR
{
    public class BookHub : Hub
    {
        ILogic logic;
        public BookHub()
        {
            logic = new RealLogic();
        }

        public void DelBook(int id)
        {
            logic.DelBook(id);
            Clients.All.Refresh();
        }

        public void AddBook(BookDTO b)
        {
            Book book= BookDTO.Mapper.Map<BookDTO, Book>(b);
            logic.AddBook(book);
            Clients.All.NewBook(b);
        }

        public IEnumerable<BookDTO> GetAllBooks()
        {
            List<Book> list = logic.GetBooks();
            return BookDTO.Mapper.Map<List<Book>, IEnumerable<BookDTO>>(list);
        }
    }
}
