﻿using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalR
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:8088";
            using (WebApp.Start(url))
            {
                Console.WriteLine("server up");
                Console.ReadLine();
            }

        }
    }
}
