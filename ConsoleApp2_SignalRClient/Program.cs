﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2_SignalRClient
{
    class Book
    {
        public int Bookno { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }

        public override string ToString()
        {
            return $"{Bookno} {Title} {Author}";
        }
    }

    class Program
    {
        static void RefreshList(IWebProxy signalr)
        {
            Console.WriteLine("FETCHING FROM SIGNALR");
            var list = signalr.Invoke<IEnumerable<Book>>("GetAllBooks").Result;

            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("press enter");
            Console.ReadLine();

            string url = "http://localhost:8088";
            var hubConnection = new IDbConnection(url);
            var hubProxy = hubConnection.CreateHubProxy("BookHub");

            hubProxy.On("Refresh", () =>
            {
                Console.WriteLine("Refresh call from server");
                RefreshList(hubProxy);
            });

            hubProxy.On<Book>("NewBook", param =>
            {
                Console.WriteLine("NEW BOOK FROM SERVER" + param);
            });

            hubConnection.Start().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("ERROR" + task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("CONNECTED");
                }
            }).Wait();
            Console.ReadLine();

            RefreshList(hubProxy);
            Console.ReadLine();

            Console.WriteLine("adding");
            Book b = new Book()
            {
                Bookno= 67,
                Title= "signalR",
                Author = "Hung"
            };
            hubProxy.Invoke("AddBook", b).Wait();
            Console.WriteLine("added");

            RefreshList(hubProxy);
            Console.ReadLine();

            Console.WriteLine("deleting");
            hubProxy.Invoke("DelBook", b.Bookno).Wait();
            Console.WriteLine("deleted");
            Console.ReadLine();

            hubConnection.Stop();
            Console.WriteLine("stopped");
            Console.ReadLine();
        }
    }
}
