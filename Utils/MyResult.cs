﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utils
{
    public class MyResult : IEnumerable<Dictionary<string, object>>
    {
        List<Dictionary<string, object>> rows;
        public MyResult()
        {
            rows = new List<Dictionary<string, object>>();
        }
        public void AddRow(string[] fields, object[] values)
        {
            Dictionary<string, object> row = new Dictionary<string, object>();
            for (int i = 0; i < fields.Length; i++)
            {
                row.Add(fields[i], values[i]);
            }
            rows.Add(row);
        }
        public IEnumerator<Dictionary<string, object>> GetEnumerator()
        {
            return rows.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {  
            return this.GetEnumerator();
        }
    }
}