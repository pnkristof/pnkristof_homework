﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4_WebClient
{
    class Book
    {
        public int Bookno { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public override string ToString()
        {
            return $"#{Bookno}: {Title} by {Author}";
        }

        class Program
        {
            static void Main(string[] args)
            {
                string url = "http://localhost:50430/api/book/";
                Console.WriteLine("waiting...");
                Console.ReadLine();

                // HttpWebRequest
                using (WebClient client = new WebClient())
                {
                    string json = client.DownloadString(url + "all");
                    var list = JsonConvert.DeserializeObject<List<Book>>(json);
                    foreach (var item in list)
                    {
                        Console.WriteLine(item.ToString());
                    }

                    //    NameValueCollection postData;
                    //    byte[] responseBytes;

                    //    postData = new NameValueCollection();

                    //    postData.Add("bookno", "0");
                    //    postData.Add("title", "Book");
                    //    postData.Add("author", "Someone");
                    //    responseBytes = client.UploadValues(url + "add", "POST", postData);
                    //    Console.WriteLine("ADD: " + Encoding.UTF8.GetString(responseBytes));
                    //    Console.WriteLine("ALL: " + client.DownloadString(url + "all"));

                    //    postData = new NameValueCollection();

                    //    postData.Add("bookno", "41");
                    //    postData.Add("title", "Book1");
                    //    postData.Add("author", "SomeoneElse");
                    //    responseBytes = client.UploadValues(url + "mod", "POST", postData);
                    //    Console.WriteLine("MOD: " + Encoding.UTF8.GetString(responseBytes));
                    //    Console.WriteLine("ALL: " + client.DownloadString(url + "all"));

                    //    Console.WriteLine("DEL: " + client.DownloadString(url + "del/41"));
                    //    Console.WriteLine("ALL: " + client.DownloadString(url + "all"));
                    //}

                    Console.WriteLine("Done");
                    Console.ReadLine();



                }
            }
        }
    }
}
