﻿using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Soap
{
    [ServiceBehavior(IncludeExceptionDetailInFaults = true, ConcurrencyMode = ConcurrencyMode.Single, InstanceContextMode = InstanceContextMode.PerSession)]
    public class BookService : IBookService
    {
        ILogic logic;
        public BookService()
        {
            logic = new RealLogic();
        }



        public bool AddBook(BookDTO book)
        {
            Book b = BookDTO.Mapper.Map<BookDTO, Book>(book);
            logic.AddBook(b);
            return true;
            
        }

        public bool DelBook(int bookno)
        {
            logic.DelBook(bookno);
            return true;
        }

        public List<BookDTO> GetBooks()
        {
            List<Book> books = logic.GetBooks();
            return BookDTO.Mapper.Map<List<Book>, List<BookDTO>>(books);
        }
    }
}
