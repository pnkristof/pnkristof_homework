﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Soap
{
    [ServiceContract]
    public interface IBookService
    {

        [OperationContract]
        List<BookDTO> GetBooks();

        [OperationContract]
        bool DelBook(int bookno);

        [OperationContract]
        bool AddBook(BookDTO book);


    }
}
