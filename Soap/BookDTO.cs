﻿using AutoMapper;
using Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Soap
{
    [DataContract]
    public class BookDTO
    {
        public static IMapper Mapper { get; private set; }
        static BookDTO()
        {
            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<BookDTO, Book>().ReverseMap();
            }).CreateMapper();
        }

        [DataMember]
        public int Bookno { get; set; }
        [DataMember]
        public string Title { get; set; }
        [DataMember]
        public string Author { get; set; }
    }
}
